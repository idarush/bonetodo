var mongoose = require('mongoose'),
	config = require('../config'),
	Q = require("q"),
	fs = require('fs'),
	path = require('path'),
	models = {};

/**
 * Setup db schemas from corresponding application files
 */
function setupModels() {
	var deferred = Q.defer(),
		dir = path.join(__dirname, "schemas");

	// read schemas directory content
	fs.readdir(dir, function (err, files) {
		if (err) {
			deferred.reject(err);
		} else {
			// load each model into application with require call
			files.forEach(function (file) {
				var fullPath = path.join(dir, file),
					name = path.basename(file, '.js');

				models[name] = require(fullPath);
			});
			deferred.resolve();
		}
	});

	return deferred.promise;
}

/**
 * Initialize DAL layer
 */
function initialize() {
	var deferred = Q.defer(),

		// prepare db connection options from config
		dbConfig = config.get('database'),
		uri = 'mongodb://' + dbConfig.host + ':' + dbConfig.port + '/' + dbConfig.db,
		options = {
			user: dbConfig.user,
			pass: dbConfig.password
		},
		connection;

	// connect to db
	mongoose.connect(uri, options);
	connection = mongoose.connection;

	connection.on('error', function (error) {
		deferred.reject(error);
	});
	// successfully connected
	connection.once('open', function () {
		setupModels()
			.then(function () {
				deferred.resolve();
			})
			.catch(function (err) {
				deferred.reject(err);
			});
	});

	return deferred.promise;
}

/**
 * Take model class by name
 */
function getModel(name) {
	return models[name];
}


module.exports.initialize = initialize;
module.exports.getModel = getModel;