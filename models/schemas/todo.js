var mongoose = require('mongoose');

// describe todo model
var TodoSchema = new mongoose.Schema({
	listId: { type: mongoose.Schema.Types.ObjectId, required: true },
	name: { type: String, required: true },
	created: { type: Date, default: Date.now },
	completed: { type: Boolean, default: false },
	order: { type: Number, required: true }
});

var model = mongoose.model('todos', TodoSchema);

// configure validation
model.schema.path('name').validate(function (value) {
	return (value || '').length < 256;
}, 'Todo name is too long. Maximum allowed length is 256 symbols.');
model.schema.path('order').validate(function (value) {
	return value > 0;
}, 'Todo order index must be positive integer');

module.exports = model;