var mongoose = require('mongoose');

// describe list schema
var ListSchema = new mongoose.Schema({
	name: {type: String, required: true},
	created: { type: Date, default: Date.now }
});

var model = mongoose.model('lists', ListSchema);

// configure validation
model.schema.path('name').validate(function (value) {
	return (value || '').length < 256;
}, 'List name is too long. Maximum allowed length is 256 symbols.');

module.exports = model;