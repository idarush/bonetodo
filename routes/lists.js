var express = require('express'),
	getModel = require('../models/db').getModel,
	router = express.Router();

/**
 * Get all
 */
router.get('/', function (request, response, next) {
	var List = getModel('list');

	List.
		find({}).
		sort({created: -1}).
		exec(function (err, list) {
			if (err) {
				return next(err);
			}
			response.json(list);
		});
});

/**
 * Get one
 */
router.get('/:id', function (request, response, next) {
	var id = request.params.id,
		List = getModel('list');

	List.findById(id, function (err, list) {
		if (err) {
			return next(err);
		}
		response.json(list);
	});
});

/**
 * Update one
 */
router.put('/:id', function (request, response, next) {
	var id = request.params.id,
		List = getModel('list');

	List.findByIdAndUpdate(id, request.body, {runValidators: true, new: true}, function (err, saved) {
		if (err) {
			return next(err);
		}
		response.json(saved);
	});
});

/**
 * Insert one
 */
router.post('/', function (request, response, next) {
	var List = getModel('list');

	List.create(request.body, function (err, post) {
		if (err) {
			return next(err);
		}
		response.json(post);
	});
});

/**
 * Delete one
 */
router.delete('/:id', function (request, response, next) {
	var Todo = getModel('todo'),
		List = getModel('list');

	List.findByIdAndRemove(request.params.id, function (err) {
		if (err) {
			return next(err);
		}
		Todo.remove({listId: request.params.id}, function (err) {
			if (err) {
				return next(err);
			}
			response.send(true);
		});
	});
});

module.exports = router;
