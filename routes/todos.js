var express = require('express'),
	_ = require('underscore'),
	Q = require('q'),
	getModel = require('../models/db').getModel,
	router = express.Router();

/**
 * Get all
 */
router.get('/', function (request, response, next) {
	var Todo = getModel('todo');

	Todo.
		find({}).
		sort({created: -1}).
		exec(function (err, list) {
			if (err) {
				return next(err);
			}
			response.json(list);
		});
});

/**
 * Get all by parent list
 */
router.get('/list/:id', function (request, response, next) {
	var id = request.params.id,
		Todo = getModel('todo');

	Todo.
		find({listId: id}).
		sort({order: 1}).
		exec(function (err, list) {
			if (err) {
				return next(err);
			}
			response.json(list);
		});
});

/**
 * Get one
 */
router.get('/:id', function (request, response, next) {
	var id = request.params.id,
		Todo = getModel('todo');

	Todo.findById(id, function (err, list) {
		if (err) {
			return next(err);
		}
		response.json(list);
	});
});

/**
 * Update one
 */
router.put('/:id', function (request, response, next) {
	var id = request.params.id,
		Todo = getModel('todo'),
		List = getModel('list'),
		body = request.body || {};

	if (!body.listId) {
		return next(new Error("Parent list id required."));
	}

	List.findById(body.listId, function (err, list) {
		if (err) {
			return next(err);
		}
		if (!list) {
			return next(new Error("Parent list not found."));
		}

		Todo.findByIdAndUpdate(id, body, {runValidators: true, new: true}, function (err, saved) {
			if (err) {
				return next(err);
			}
			response.json(saved);
		});
	});
});

/**
 * Update all
 */
router.put('/', function (request, response, next) {
	var Todo = getModel('todo'),
		List = getModel('list'),
		collection = request.body || [],
		parents = _.unique(_.map(collection, function (item) {
			return item.listId;
		}));


	List.
		find({_id: {$in: [parents]}}).
		exec(function (err, list) {
			if (err) {
				return next(err);
			}
			if (list.length != parents.length) {
				return next(new Error("Parent list not found."));
			}

			var promises = _.map(collection, function (item) {
				var defer = Q.defer();

				Todo.findByIdAndUpdate(item._id, item, {runValidators: true, new: true}, function (err, saved) {
					if (err) {
						defer.reject(err);
					} else {
						defer.resolve(saved);
					}
				});

				return defer.promise;
			});

			Q.allSettled(promises)
				.then(function (results) {
					var errors = [];
					results.forEach(function (result) {
						if (result.state !== "fulfilled") {
							errors.push(result.reason);
						}
					});
					if (errors.length) {
						return next(errors);
					}
					response.send(true);
				});
		});
});

/**
 * Insert one
 */
router.post('/', function (request, response, next) {
	var Todo = getModel('todo'),
		List = getModel('list'),
		body = request.body || {};

	if (!body.listId) {
		return next(new Error("Parent list id required."));
	}

	List.findById(body.listId, function (err, list) {
		if (err) {
			return next(err);
		}
		if (!list) {
			return next(new Error("Parent list not found."));
		}

		Todo.create(body, function (err, post) {
			if (err) {
				return next(err);
			}
			response.json(post);
		});
	});
});

/**
 * Delete one
 */
router.delete('/:id', function (request, response, next) {
	var Todo = getModel('todo');

	Todo.findByIdAndRemove(request.params.id, function (err) {
		if (err) {
			return next(err);
		}
		response.send(true);
	});
});

module.exports = router;
