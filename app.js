var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var todos = require('./routes/todos');
var lists = require('./routes/lists');

var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/lists', lists);
app.use('/todos', todos);

/**
 * Catch 404 and forward to error handler
 */
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

/**
 * Prepare error object
 */
function processError(err) {
	var result;

	if (err.errors) {
		result = {};
		// if error is complex
		// collect messages into simple object
		for (var p in err.errors) {
			if (err.errors.hasOwnProperty(p)) {
				result[p] = err.errors[p].message;
			}
		}
	} else {
		result = err;
	}

	return result;
}
/**
 * Error middleware
 */
app.use(function (err, req, res) {
	res.status(err.status || 500);
	res.send({
		message: err.message,
		error: processError(err)
	});
});


module.exports = app;
