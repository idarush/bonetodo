/**
 * Common application constants
 */
define(function(){

	return {
		cleanupViewEvent: 'cleanup',
		errorPrefix: 'error:'
	};

});