/**
 * Todos collection.
 */
define(function (require) {

	var Backbone = require('Backbone'),
		Model = require('app/models/entities/todo'),
		_ = require('underscore'),
		constants = require('app/constants');

	return Backbone.Collection.extend({
		url: Model.url,
		model: Model,

		// default comparator.
		comparator: 'order',

		/**
		 * Update ordering by item status.
		 */
		_restoreOrder: function (item, event) {
			var isActive = item.get('completed'),
				newIndex = 1,
				cid = item.cid,
				self = this;

			this.forEach(function (elem) {
				if (elem.get('completed') == isActive) {
					if (cid != elem.cid) {
						elem.set({
							order: newIndex,
							// save previous order for restore model state after error
							prevOrder: item.get('order')
						});
					}
					newIndex++;
				}
			});

			// send data to the server
			Backbone.sync("update", this, {
				success: function () {
					self.forEach(function (elem) {
						// clean backup
						elem.unset('prevOrder');
					});
					self.sort();
					self.trigger(event, item);
				},
				error: function () {
					// restore models state
					self.forEach(function (elem) {
						if (elem.get('completed') == isActive) {
							elem.set({
								order: elem.get('prevOrder'),
								prevOrder: undefined
							});
						}
					});
					self.trigger(constants.errorPrefix + event, item);
				}
			});
		},


		/**
		 * Get order number for new item depending on it state.
		 */
		getNewItemOrder: function (isActive) {
			var subItems = this.where({completed: !isActive});
			return subItems.length + 1;
		},

		/**
		 * Change item order number with offset.
		 */
		moveItem: function (item, offset) {
			var isActive = item.get('completed'),

				// ordering works inside the same status items collection
				subItems = this.where({completed: isActive}),
				index = _.findIndex(subItems, function (i) {
					return i.cid == item.cid;
				}),
				newPosition = index + offset,
				event = 'moved',
				self = this;

			// check out of range error
			newPosition = Math.min(newPosition, subItems.length - 1);
			newPosition = Math.max(newPosition, 0);

			// move item into new position
			subItems.splice(index, 1);
			subItems.splice(newPosition, 0, item);

			// apply new order numbers
			_.each(subItems, function (item, i) {
				item.set({
					order: i + 1,
					// save previous order for restore model state after error
					prevOrder: item.get('order')
				});
			});

			Backbone.sync("update", this, {
				success: function () {
					// clean saved previous order number
					self.forEach(function (elem) {
						elem.unset('prevOrder');
					});
					self.sort();
					self.trigger(event, item);
				},
				error: function () {
					// restore models state
					self.forEach(function (elem) {
						elem.set({
							order: elem.get('prevOrder'),
							prevOrder: undefined
						});
					});
					self.trigger(constants.errorPrefix + event, item);
				}
			});
		},

		/**
		 * Change todo status.
		 */
		setItemState: function (item, completed) {
			var self = this,
				event = "change-state",
				activeIndex = 1,
				completedIndex = 1;

			// move item at the beginning of the collection
			item.set({
				'completed': !!completed,
				'order': -1,
				prevOrder: item.get('order')
			});

			this.sort();
			// one item change it's state - change orders for collection
			this.forEach(function (elem) {
				if (!elem.get('completed')) {
					elem.set({
						order: activeIndex++,
						// save previous order for restore model state after error
						prevOrder: elem.get('order')
					});
				} else {
					elem.set({
						order: completedIndex++,
						// save previous order for restore model state after error
						prevOrder: elem.get('order')
					});
				}
			});

			Backbone.sync("update", this, {
				success: function () {
					self.forEach(function (elem) {
						elem.unset('prevOrder');
					});
					self.trigger(event, item);
				},
				error: function () {

					// restore models state
					item.set({completed: !completed}, {silent: true});
					self.forEach(function (elem) {
						elem.set({
							order: elem.get('prevOrder'),
							prevOrder: undefined
						});
					});

					// restore models ordering
					self.sort();
					self.trigger(constants.errorPrefix + event, item);
				}
			});
		},


		/**
		 * Remove todos list (call api).
		 */
		deleteItem: function (id) {
			var model = this.get(id),
				event = 'delete-item',
				self = this;

			if(!model) {
				return false;
			}

			model.destroy({
				wait: true,
				success: function () {
					// save new orders after removing
					self._restoreOrder(model, event);
				},
				error: function () {
					self.trigger(constants.errorPrefix + event, model);
				}
			});
		}
	});

});