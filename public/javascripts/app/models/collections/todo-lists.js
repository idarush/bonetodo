/**
 * Todos list collection.
 */
define(function(require){

	var Backbone = require('Backbone'),
		Model = require('app/models/entities/todo-list'),
		constants = require('app/constants');

	return Backbone.Collection.extend({
		url: Model.url,
		model: Model,

		/**
		 * Default comparator. Sort by creation date desc.
		 */
		comparator: function(list) {
			return -list.get('created').getTime();
		},

		/**
		 * Set selected item for collection.
		 */
		setSelected: function(model) {
			if (this.selected) {
				this.selected.set({ selected: false });
			}
			if(model) {
				model.set({selected: true});
			}
			this.selected = model;
			this.trigger('selected', model);
		},

		/**
		 * Delete item (call api).
		 */
		deleteItem: function (id) {
			var model = this.get(id),
				event = 'delete-item',
				self = this;

			if(!model) {
				return false;
			}

			model.destroy({
				wait: true,
				success: function () {
					self.trigger(event, model);
				},
				error: function () {
					self.trigger(constants.errorPrefix + event, model);
				}
			});
		}
	});

});