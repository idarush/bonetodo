/**
 * Todos list entity.
 */
define(function(require){

	var Base = require('app/models/entities/base'),
		url = '/lists';

	var model = Base.extend({
		urlRoot: url,

		/**
		 * Set selected flag for list.
		 */
		setSelected:function() {
			if(this.collection) {
				this.collection.setSelected(this);
			}
		},

		// convert string date representation into Date object (for sorting)
		parse: function () {
			var data = Base.prototype.parse.apply(this, arguments);
			if(data.created && typeof data.created == "string") {
				data.created = new Date(data.created);
			}
			return data;
		}
	});

	model.url = url;
	return model;
});