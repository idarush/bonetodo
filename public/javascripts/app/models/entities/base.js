/**
 * Base entity.
 */
define(function(require){

	var Backbone = require('Backbone');

	// configure id property mapping
	return Backbone.Model.extend({
		idAttribute: "_id"
	});
});