/**
 * Todos entity.
 */
define(function(require){

	var Base = require('app/models/entities/base'),
		url = '/todos';

	var model = Base.extend({
		urlRoot: url,

		// fill 'complete' field with boolean value
		parse: function () {
			var data = Base.prototype.parse.apply(this, arguments);
			data.completed = !!data.completed;
			return data;
		}
	});

	model.url = url;
	return model;
});