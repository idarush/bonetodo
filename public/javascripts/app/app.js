/**
 * Application.
 */
define(function (require) {

	var Backbone = require('Backbone'),
		Router = require('app/routing/todo'),
		$ = require('jquery');


	var routers = {};
	return {
		run: run
	};

	/////////////////////////

	function run() {
		// load templates and start hash change handling
		$.ajax('/templates/all.html')
			.done(function (templates) {
				$('head').prepend(templates);
				routers.todo = new Router();
				Backbone.history.start();
			});
	}
});
