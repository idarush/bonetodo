/**
 * require.js configuration and application run
 */
(function (requirejs) {
	'use strict';

	requirejs.config({
		baseUrl: '/javascripts',
		shim: {
			underscore: {
				exports: '_'
			},
			Backbone: {
				deps: ["underscore", "jquery"],
				exports: "Backbone"
			},
			'jquery.validate': {
				deps: ["jquery"]
			},
			bootstrap: {
				deps: ["jquery"]
			}
		},
		paths: {
			Sortable: 'libs/Sortable',
			underscore: 'libs/underscore',
			Backbone: 'libs/backbone',
			jquery: 'libs/jquery-2.1.4.min',
			'jquery.validate': 'libs/jquery.validate',
			bootstrap: 'libs/bootstrap',
			toastr: 'libs/toastr'
		}
	});

	/**
	 * Load jquery plugins and Bootstrap before application run
	 */
	var deps = [
		'jquery.validate', 'libs/jquery-ui',
		'bootstrap', 'app/app'
	];
	requirejs(deps, function(v, u, b, app) {
		app.run();
	});

})(requirejs);
