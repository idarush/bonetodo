define(function(require){

	var Backbone = require('Backbone'),
		$ = require('jquery'),
		MainView = require('app/views/main');

	/**
	 * Default application router.
	 */
	return Backbone.Router.extend({

		routes: {
			"": "index"
		},

		index: function() {
			var view = new MainView();
			$('body').append(view.render().$el);
		}
	});
});
