/**
 * Main application view.
 */
define(function (require) {
	var BaseView = require('app/views/base'),
		constants = require('app/constants'),
		TaskListView = require('app/views/lists'),
		$ = require('jquery'),
		template = null;

	return BaseView.extend({

		tagName: "div",
		className: "todo",
		events: {
			'click .add': 'add'
		},

		/**
		 * Ann new todos list event handler.
		 */
		add: function () {
			this.trigger('add');
		},

		initialize: function() {
			if(!template) {
				template = $('#container-template').html();
			}

			this.taskListView = new TaskListView();
			this.taskListView.listenToOnce(this, constants.cleanupViewEvent, this.taskListView.remove);
			// forward add event to child view
			this.taskListView.listenTo(this, 'add', this.taskListView.add);
		},

		render: function() {
			this.$el.html(template);
			this.$el.find('#master').append(this.taskListView.render().$el);
			return this;
		}
	});

});
