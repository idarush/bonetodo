/**
 * Base view.
 */
define(function (require) {

	var Backbone = require('Backbone'),
		constants = require('app/constants'),
		$ = require('jquery');

	return Backbone.View.extend({

		/**
		 * Notify all children views with 'remove' event.
		 */
		removeInnerViews: function () {
			this.trigger(constants.cleanupViewEvent);
		},

		remove: function () {
			var base = Backbone.View.prototype.remove;
			// remove all children views as well
			this.removeInnerViews();
			base.call(this);
		},

		/**
		 * Simple helper to apply blink animation to element.
		 */
		blinkElement: function ($el, color) {
			color = color || '#5cb85c';
			$el.effect("highlight", {color: color}, 400);
		}
	});

});
