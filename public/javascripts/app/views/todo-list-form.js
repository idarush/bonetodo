/**
 * Modal form for edit or create single todo list.
 */
define(function (require) {

	var Backbone = require('Backbone'),
		BaseView = require('app/views/base'),
		_ = require('underscore'),
		$ = require('jquery'),
		constants = require('app/constants'),
		toastr = require('toastr'),
		template = null;

	return BaseView.extend({

		tagName: "div",
		className: "todo-list-form",
		mode: 'cancel',
		events: {
			'submit form': 'save',
			'click .cancel': 'cancel'
		},

		/**
		 * Cancel event handler.
		 */
		cancel: function () {
			this.mode = 'cancel';
			// restore model state
			this.model.set(this.initialData, {silent: true});
			this.close();
		},

		/**
		 * Close window.
		 */
		close: function () {
			$(this.$el.find('> .modal')).modal('hide');
		},

		/**
		 * Collect user input.
		 */
		collectData: function () {
			return {
				name: this.$el.find('#todo-list-name').val().trim()
			};
		},

		/**
		 * Save event handler.
		 */
		save: function (e) {
			// prevent form submitting
			e.preventDefault();

			this.mode = 'ok';
			this.model.set(this.collectData());

			this.model.save(null, {
				success: _.bind(function () {
					this.close();
				}, this),
				error: function () {
					toastr.error('Unable to save this todo list.');
				}
			});
		},

		initialize: function () {
			if (!template) {
				template = _.template($('#todo-list-form-template').html());
			}

			// backup model state to restore after error
			this.initialData = JSON.parse(JSON.stringify(this.model.toJSON()));
		},

		render: function () {
			$(this.$el).data('cid', this.model.cid);
			this.$el.html(template({
				list: this.model.toJSON(),
				isNew: this.model.isNew()
			}));

			var self = this;
			$(this.$el.find('> .modal')).modal({
				keyboard: false,
				backdrop: 'static'
			}).on('hidden.bs.modal', function () {
				$(this).data('bs.modal', null);
				self.trigger(self.mode, self, self.mode);
			});

			// configure form validation
			$(this.$el.find('form')).validate({
				rules: {
					"todo-list-name": {
						required: true,
						maxlength: 256
					}
				}
			});

			return this;
		},

		remove: function () {
			var base = BaseView.prototype.remove;
			// destroy jquery plugins
			this.$el.find('form').validate('destroy');
			base.call(this);
		}
	});
});