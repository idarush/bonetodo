/**
 * Single todos list.
 */
define(function (require) {

	var BaseView = require('app/views/base'),
		_ = require('underscore'),
		$ = require('jquery'),
		constants = require('app/constants'),
		DetailsView = require('app/views/todos'),
		template = null;

	return BaseView.extend({

		tagName: "a",
		className: "todo-lists-item list-group-item",
		attributes: {
			href: 'javascript:void(0)'
		},
		events: {
			'click .add': 'add'
		},

		/**
		 * Add new todo into list event hadler.
		 */
		add: function (e) {
			e.stopPropagation();
			this.trigger('add');
		},

		initialize: function () {
			if(!template) {
				template = _.template($('#todo-list-item-template').html());
			}

			// listen model events
			this.listenTo(this.model, 'change:selected', this._select);
			this.listenTo(this.model, 'sync', this._render);
			this.container = $('#details');
		},

		// update view when user select or deselect item
		_select: function (m, selected) {
			this.render();
			this.removeInnerViews();

			if(selected) {
				var view = new DetailsView({
					model: this.model.get('_id')
				});
				view.listenToOnce(this, constants.cleanupViewEvent, view.remove);
				// forward add event to todos list view
				view.listenTo(this, 'add', view.add);
				this.container.html('').append(view.render().$el);
			}
		},

		_render: function (blink) {
			if(this.model.get('selected')) {
				this.$el.addClass('list-group-item-info');
			} else {
				this.$el.removeClass('list-group-item-info');
			}
			this.$el.data('cid', this.model.cid);
			this.$el.html(template({
				item: this.model.toJSON()
			}));
			if(blink) {
				// blink element after form was closed
				setTimeout(_.bind(function () {
					this.blinkElement(this.$el);
				}, this), 50);
			}
			return this;
		},

		render: function () {
			return this._render();
		}
	});
});