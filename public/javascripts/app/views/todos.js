/**
 * Todos view.
 */
define(function (require) {

	var Backbone = require('Backbone'),
		BaseView = require('app/views/base'),
		Collection = require('app/models/collections/todos'),
		_ = require('underscore'),
		$ = require('jquery'),
		constants = require('app/constants'),
		ItemView = require('app/views/todo'),
		FormView = require('app/views/todo-form'),
		Sortable = require('Sortable'),
		toastr = require('toastr'),
		template = null;

	return BaseView.extend({

		tagName: "div",
		className: "",
		events: {
			'click .edit': 'edit',
			'click .delete': 'delete',
			'click .done': 'changeState',
			'click .start': 'changeState'
		},

		/**
		 * Show form for todo name edit
		 */
		_initForm: function (model) {
			var isNew = model.isNew(),
				view = new FormView({
					model: model
				});

			view.listenToOnce(this, constants.cleanupViewEvent, function () {
				this.stopListening(view);
				view.remove();
			});
			// wait user action
			this.listenToOnce(view, 'cancel ok', function (v, evt) {
				view.remove();

				// if new model was saved
				// add new item into collection
				// and display it
				if (evt == 'ok' && isNew) {
					this.collection.add(model);
					this.render();
				}
			});

			$('body').append(view.render().$el);
		},

		/**
		 * Delete todo event handler.
		 */
		delete: function (e) {
			e.stopPropagation();
			var id = $(e.currentTarget).data('id');

			if (confirm("Are you sure yoy want to remove this todo?")) {
				this.collection.deleteItem(id);
			}
		},

		/**
		 * Change todo status event handler.
		 */
		changeState: function (e) {
			var $e = $(e.currentTarget),
				$parent = $e.closest('li'),
				id = $e.data('id'),
				completed = $e.hasClass('done'),
				model = this.collection.get(id),
				self = this;

			if (model) {
				// start hide animation
				$parent.fadeTo(500, 0, function () {
					// save new status
					self.collection.setItemState(model, completed);
				});
			}
		},

		/**
		 * Add todo event handler.
		 */
		add: function () {
			var model = new this.collection.model({
				completed: false,
				order: this.collection.getNewItemOrder(true),
				listId: this.model
			});
			this._initForm(model);
		},

		/**
		 * Change todo event handler.
		 */
		edit: function (e) {
			var id = $(e.currentTarget).data('id'),
				model = this.collection.get(id);

			if (model) {
				this._initForm(model);
			}
		},

		/**
		 * Configure sortable plugin.
		 */
		initPlugins: function ($el) {
			var self = this;

			this.sortable = Sortable.create($el[0], {
				onEnd: function (evt) {
					// if item was really moved
					if (typeof evt.newIndex != "undefined") {
						var offset = evt.newIndex - evt.oldIndex;
						if (offset != 0) {
							var cid = $(evt.item).data('cid'),
								item = self.collection.get(cid);

							// save new position
							if (item) {
								self.collection.moveItem(item, offset);
							}
						}
					}
				},
				ghostClass: "sortable-ghost"
			});
		},

		/**
		 * Clean ui plugins.
		 */
		destroyPlugins: function () {
			if (this.sortable) {
				this.sortable.destroy();
				this.sortable = null;
			}
		},

		initialize: function () {
			if (!template) {
				template = $('#todo-list-template').html();
			}

			this.collection = new Collection();
			var self = this;

			// load data from server
			Backbone.sync("read", this.collection, {
				url: this.collection.url + "/list/" + this.model,
				success: function (data) {
					self.collection.reset(data, {silent: true});
					self.collection.trigger("read");
				},
				error: function () {
					toastr.error('Unable to load todos.');
				}
			});
			this.listenTo(this.collection, 'read moved delete-item change-state', this.render);

			// listen errors
			this.listenTo(this.collection, constants.errorPrefix + 'delete-item', function () {
				toastr.error('Unable to remove todo.');
			});
			this.listenTo(this.collection, constants.errorPrefix + 'change-state', function () {
				toastr.error('Unable to change todo status.');
				this.render();
			});
			this.listenTo(this.collection, constants.errorPrefix + 'moved', function () {
				toastr.error('Unable to save new todo order.');
				this.render();
			});
		},

		_render: function() {
			this.removeInnerViews();
			this.$el.html(template);

			var self = this,

				active = this.collection.where({completed: false}),
				completed = this.collection.where({completed: true}),

				$activeContainer = this.$el.find('.active-list'),
				$completedContainer = this.$el.find('.completed-list'),

				iterative = [
					{data: active, $el: $activeContainer},
					{data: completed, $el: $completedContainer}
				];

			// split todos into two collections by status
			_.each(iterative, function (item) {
				_.each(item.data, function (todo) {
					var view = new ItemView({
						model: todo
					});
					view.listenTo(self, constants.cleanupViewEvent, view.remove);
					item.$el.append(view.render().$el);
				})
			});

			// init sortable plugin only for active todos
			if (active.length) {
				this.initPlugins($activeContainer);
			}
			return this;
		},

		render: function () {
			return this._render();
		},

		remove: function () {
			var base = BaseView.prototype.remove;
			this.destroyPlugins();
			base.call(this);
		}
	});
});