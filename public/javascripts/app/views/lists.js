define(function (require) {

	var BaseView = require('app/views/base'),
		Collection = require('app/models/collections/todo-lists'),
		_ = require('underscore'),
		$ = require('jquery'),
		constants = require('app/constants'),
		ItemView = require('app/views/list'),
		FormView = require('app/views/todo-list-form'),
		toastr = require('toastr');

	/**
	 * View for collection of todos list.
	 */
	return BaseView.extend({

		tagName: "div",
		className: "todo-lists list-group",

		events: {
			'click .todo-lists-item': 'selectItem',
			'click .edit': 'edit',
			'click .delete': 'delete'
		},

		/**
		 * Show form for list name edit
		 */
		_initForm: function (model) {
			var isNew = model.isNew(),
				view = new FormView({
					model: model
				});

			view.listenToOnce(this, constants.cleanupViewEvent, function () {
				this.stopListening(view);
				view.remove();
			});

			// wait user action
			this.listenToOnce(view, 'cancel ok', function (v, evt) {
				view.remove();
				// if new model was saved
				// add new item into collection
				// and select it
				if (evt == 'ok' && isNew) {
					this.collection.add(model);
					this.render();
					this._selectItem(model.cid);
				}
			});

			$('body').append(view.render().$el);
		},

		/**
		 * Delete list event handler.
		 */
		delete: function (e) {
			e.stopPropagation();

			var id = $(e.currentTarget).data('id'),
				model = this.collection.get(id);

			if (model && confirm("Are you sure you want to delete the whole todo list?")) {
				// backup previous user selection
				this._savedSelection = this.collection.selected;
				this.collection.deleteItem(id);
			}
		},

		/**
		 * Add list event handler.
		 */
		add: function () {
			var model = new this.collection.model({
				selected: false
			});
			this._initForm(model);
		},

		/**
		 * Change list event handler.
		 */
		edit: function (e) {
			e.stopPropagation();
			var id = $(e.currentTarget).data('id'),
				model = this.collection.get(id);

			if (model) {
				this._initForm(model);
			}
		},

		/**
		 * Set selected list.
		 */
		_selectItem: function (cid) {
			var self = this;
			this.collection.forEach(function (taskList) {
				if (taskList.cid == cid) {
					self.collection.setSelected(taskList);
				}
			});
		},

		/**
		 * Item click event handler.
		 */
		selectItem: function (e) {
			var cid = $(e.currentTarget).data('cid');
			this._selectItem(cid);
		},

		initialize: function () {
			this.collection = new Collection();
			this.listenTo(this.collection, 'reset', function () {
				this.render();
				this.$el.find('.todo-lists-item ').first().trigger('click');
			});
			this.listenTo(this.collection, 'delete-item', function (item) {
				this.render();
				// try to restore user selection
				if (this._savedSelection) {
					if (item.cid == this._savedSelection.cid) {
						this.collection.setSelected(this.collection.at(0) || null );
					} else {
						this.collection.setSelected(this._savedSelection);
					}
					delete this._savedSelection;
				}
			});

			// errors
			this.listenTo(this.collection, constants.errorPrefix + 'delete-item', function () {
				toastr.error('Unable to remove todo list.');
				this.render();
			});

			this.collection.fetch({reset: true});
		},

		render: function () {
			this.removeInnerViews();
			this.$el.html('');

			var self = this;
			this.collection.forEach(function (taskList) {
				var view = new ItemView({
					model: taskList
				});
				view.listenToOnce(self, constants.cleanupViewEvent, view.remove);
				self.$el.append(view.render().$el);
			});

			return this;
		}
	});
});