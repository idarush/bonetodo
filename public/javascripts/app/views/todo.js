/**
 * Single todo item view.
 */
define(function (require) {

	var BaseView = require('app/views/base'),
		_ = require('underscore'),
		$ = require('jquery'),
		constants = require('app/constants'),
		template = null;

	return BaseView.extend({

		tagName: "li",
		className: "todo list-group-item",

		initialize: function () {
			if(!template) {
				template = _.template($('#todo-item-template').html());
			}
			// update view if modal was saved
			this.listenTo(this.model, 'sync', this._render);
		},

		_render: function (blink) {
			var todo = this.model.toJSON(),
				self = this;

			if(todo.completed) {
				this.$el.addClass('completed');
			} else {
				this.$el.removeClass('completed');
			}
			$(this.$el).data('cid', this.model.cid);
			this.$el.html(template({
				todo: todo
			}));

			if(blink) {
				// blink element after form was closed
				setTimeout(function () {
					self.blinkElement(self.$el);
				}, 50);
			}
			return this;
		},

		render: function () {
			return this._render();
		}
	});
});